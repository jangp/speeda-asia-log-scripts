#!/bin/sh

START_DATE=$1
END_DATE=$2

cat resources/query_reports.json | sed -e "s/START/${START_DATE}/g" -e "s/END/${END_DATE}/g" | \
  curl -H "Content-Type: application/json" -sS --data @- -XGET analyze01.ub-speeda.lan:9201/speeda_log/type1/_search | \
  jq  -r '.aggregations.report_count.buckets | map(.key |= split("?")[0]) | group_by(.key) | map({"key": .[0].key, "v":map(.doc_count)|add}) | sort_by(.key) |reverse ' | \
  sed 's/https:\/\/www.ub-speeda.com\/article\/reportarticle\/repId\///g' > resources/temp.json

jq -s '[.[][]]' resources/temp.json resources/reports.json | jq -r 'group_by(.key) | map({"key": .[0].key, "count": (if .[0].v == null then (if .[1].v == null then 0 else .[1].v end) else .[0].v end), "title": (if .[0].title == null then (if .[1].title == null then "" else .[1].title end) else .[0].title end) }) | .[]|[.key, .count, .title|tostring] | @tsv' > output/${START_DATE}_${END_DATE}_report_count.tsv
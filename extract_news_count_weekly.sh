#!/bin/sh

START_DATE=$1
END_DATE=$2

cat resources/query_news.json | sed -e "s/START/${START_DATE}/g" -e "s/END/${END_DATE}/g" | \
  curl -H "Content-Type: application/json" -sS --data @- -XGET http://localhost:9999/speeda_log/type1/_search | \
  jq  -r '.aggregations.hit_count_per_week.buckets | .[]|[.key_as_string[0:10], .doc_count] | @csv' > output/${START_DATE}_${END_DATE}_news_hit_count.csv
FROM alpine:3.10
RUN mkdir /opt/output && \
    apk add --no-cache curl jq
ADD extract_report_count.sh /opt/run.sh
RUN chmod +x /opt/run.sh
ADD resources /opt/resources
WORKDIR /opt
ENTRYPOINT ["/opt/run.sh"]
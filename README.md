# README #

### What is this repository for? ###

* This is to extract the logs for user access in SPEEDA

### How do I get set up? ###

* [install jq] (https://stedolan.github.io/jq/)
* update the reports data into reports.json by executing query to SPEEDA DB and convert results to json format with tools like: https://onlinetsvtools.com/convert-tsv-to-json
```mysql
SELECT DISTINCT english_top_page_content_id as `key`, title
FROM speeda.top_page_content;
```
* sample command: 
```bash
./extract_report_count.sh 2018-03-01 2018-03-28
./extract_news_count_weekly.sh 2019-01-01 2020-02-20

```
* sample output: 2018-03-01_2018-03-28_report_count.csv


### How do I run it on a remote machine?
* Deploy script changes:
```bash
docker-compose build && docker push docker-registry.uzabase.com/speeda/asia-log-script:1.0.0 
``` 
* sample command
```bash
docker run -v /data/log/asia-log-script/:/opt/output/ docker-registry.uzabase.com/speeda/asia-log-script:1.0.0 2019-01-01 2019-01-31
```
output tsv will be in ``/data/log/asia-log-script/`` folder